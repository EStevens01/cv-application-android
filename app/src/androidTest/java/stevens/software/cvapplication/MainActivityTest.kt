package stevens.software.cvapplication

import android.content.Intent
import android.view.Gravity
import androidx.test.InstrumentationRegistry.getTargetContext
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.DrawerActions
import androidx.test.espresso.contrib.DrawerMatchers.isClosed
import androidx.test.espresso.contrib.NavigationViewActions
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import org.hamcrest.Matchers.containsString
import org.junit.FixMethodOrder
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.MethodSorters


@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @Rule
    @JvmField
    var mainActivity: ActivityTestRule<MainActivity> =
        IntentsTestRule(MainActivity::class.java, true, false)

    private fun launchActivityWithIntent() {
        val intent = Intent()
        mainActivity.launchActivity(intent)
    }

    @Test
    fun testMainActivity() {
        launchActivityWithIntent()
        onView(withId(R.id.drawer_layout)).perform(DrawerActions.open())
        onView(withId(R.id.nav_view)).perform(NavigationViewActions.navigateTo(R.id.experience))
        onView(withId(R.id.fragmentTitle)).check(matches(isDisplayed()))
        onView(withId(R.id.fragmentTitle)).check(matches(withText(containsString("Experience"))))
        Thread.sleep(1000)

        onView(withId(R.id.drawer_layout)).perform(DrawerActions.open())
        onView(withId(R.id.nav_view)).perform(NavigationViewActions.navigateTo(R.id.projects))
        onView(withId(R.id.fragmentTitle)).check(matches(isDisplayed()))
        onView(withId(R.id.fragmentTitle)).check(matches(withText(containsString("Projects"))))
        Thread.sleep(1000)

        onView(withId(R.id.drawer_layout)).perform(DrawerActions.open())
        onView(withId(R.id.nav_view)).perform(NavigationViewActions.navigateTo(R.id.education))
        onView(withId(R.id.fragmentTitle)).check(matches(isDisplayed()))
        onView(withId(R.id.fragmentTitle)).check(matches(withText(containsString("Education"))))
        Thread.sleep(1000)

        onView(withId(R.id.drawer_layout)).perform(DrawerActions.open())
        onView(withId(R.id.nav_view)).perform(NavigationViewActions.navigateTo(R.id.about))
        onView(withId(R.id.profile_photo)).check(matches(isDisplayed()))
        Thread.sleep(1000);
    }
}