package stevens.software.cvapplication.recyclerViewAdapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import stevens.software.cvapplication.R
import stevens.software.cvapplication.model.Experience


class ExperienceRecyclerViewAdapter(context: Context?, experience: List<Experience>) : RecyclerView.Adapter<ExperienceRecyclerViewAdapter.ViewHolder>() {
    private var experiences: List<Experience>
    private val layoutInflater: LayoutInflater

    fun setExperiences(experiences: List<Experience>) {
        this.experiences = experiences
        notifyItemInserted(this.experiences.size)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView: View = layoutInflater.inflate(R.layout.list_item, parent, false)

        return ViewHolder(
            itemView
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val experience: Experience = experiences[position]
        val title: String? = experience.title
        val company: String? = experience.company
        val dateFrom: String? = experience.dateFrom
        val dateTo: String? = experience.dateTo

        holder.courseTitle.text = title
        holder.educationDescription.text = company
        holder.date.text = dateFrom + " - " + dateTo
    }

    override fun getItemCount(): Int {
        return experiences.size
    }

    class ViewHolder(viewItem: View) :
        RecyclerView.ViewHolder(viewItem) {
        var date: TextView = viewItem.findViewById(R.id.date)
        var courseTitle: TextView = viewItem.findViewById(R.id.title)
        var educationDescription: TextView = viewItem.findViewById(R.id.description)
    }

    init {
        experiences = experience
        layoutInflater = LayoutInflater.from(context)
    }
}