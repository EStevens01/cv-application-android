package stevens.software.cvapplication.recyclerViewAdapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import stevens.software.cvapplication.R
import stevens.software.cvapplication.model.Project

class ProjectRecyclerViewAdapter(context: Context?, projects: List<Project>) : RecyclerView.Adapter<ProjectRecyclerViewAdapter.ViewHolder>() {
    private var projects: List<Project>
    private val layoutInflater: LayoutInflater

    fun setProjects(projects: List<Project>) {
        this.projects = projects
        notifyItemInserted(this.projects.size)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView: View = layoutInflater.inflate(R.layout.list_item, parent, false)

        return ViewHolder(
            itemView
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val project: Project = projects[position]
        val title: String? = project.title
        val description: String? = project.description

        holder.title.text = title
        holder.description.text = description
        holder.date.visibility = View.GONE

    }

    override fun getItemCount(): Int {
        return projects.size
    }

    class ViewHolder(noteViewItem: View) :
        RecyclerView.ViewHolder(noteViewItem) {
        var title: TextView = noteViewItem.findViewById(R.id.title)
        var description: TextView = noteViewItem.findViewById(R.id.description)
        var date: TextView = noteViewItem.findViewById(R.id.date)
    }

    init {
        this.projects = projects
        layoutInflater = LayoutInflater.from(context)
    }
}