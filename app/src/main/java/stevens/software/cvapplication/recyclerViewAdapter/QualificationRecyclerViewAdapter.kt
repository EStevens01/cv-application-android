package stevens.software.cvapplication.recyclerViewAdapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import stevens.software.cvapplication.R
import stevens.software.cvapplication.model.Qualification

class QualificationRecyclerViewAdapter(context: Context?, qualifications: List<Qualification>) : RecyclerView.Adapter<QualificationRecyclerViewAdapter.ViewHolder>() {
    private var qualifications: List<Qualification>
    private val layoutInflater: LayoutInflater

    fun setQualifications(qualifications: List<Qualification>) {
        this.qualifications = qualifications
        notifyItemInserted(this.qualifications.size)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView: View = layoutInflater.inflate(R.layout.list_item, parent, false)
        return ViewHolder(
            itemView
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val qualification: Qualification = qualifications[position]
        val course: String? = qualification.course
        val description: String? = qualification.description
        val dateFrom: String? = qualification.dateFrom
        val dateTo: String? = qualification.dateTo

        holder.courseTitle.text = course
        holder.educationDescription.text = description
        holder.dateFrom.text = dateFrom + " - " + dateTo
    }

    override fun getItemCount(): Int {
        return qualifications.size
    }

    class ViewHolder(viewItem: View) :
        RecyclerView.ViewHolder(viewItem) {
        var courseTitle: TextView = viewItem.findViewById(R.id.title)
        var educationDescription: TextView = viewItem.findViewById(R.id.description)
        var dateFrom: TextView = viewItem.findViewById(R.id.date)

    }

    init {
        this.qualifications = qualifications
        layoutInflater = LayoutInflater.from(context)
    }
}