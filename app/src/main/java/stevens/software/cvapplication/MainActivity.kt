package stevens.software.cvapplication

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.Navigation
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import com.google.android.material.navigation.NavigationView
import stevens.software.cvapplication.fragment.AboutFragment
import stevens.software.cvapplication.fragment.ExperienceFragment
import stevens.software.cvapplication.fragment.ProjectsFragment
import stevens.software.cvapplication.fragment.QualificationFragment

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener  {
    private var drawer: DrawerLayout? = null
    private var toolbar: Toolbar? = null
    private var appBarConfiguration: AppBarConfiguration? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nav_main)
        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        drawer = findViewById(R.id.drawer_layout)
        val navigationView = findViewById<NavigationView>(R.id.nav_view)
        navigationView.setNavigationItemSelectedListener(this);

        appBarConfiguration = AppBarConfiguration.Builder(R.id.about, R.id.experience, R.id.projects, R.id.education)
            .setDrawerLayout(drawer)
            .build()
        val navController = Navigation.findNavController(this, R.id.nav_host_fragment)
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration!!)
        NavigationUI.setupWithNavController(navigationView, navController)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.about -> {
                val aboutFragment =
                    AboutFragment()
                this.supportFragmentManager.beginTransaction()
                    .replace(R.id.content_frame, aboutFragment)
                    .addToBackStack(null)
                    .commit()
                toolbar!!.title = item.title
            }
            R.id.education -> {
                val educationFragment =
                    QualificationFragment()
                this.supportFragmentManager.beginTransaction()
                    .replace(R.id.content_frame, educationFragment)
                    .addToBackStack(null)
                    .commit()
                toolbar!!.title = item.title
            }
            R.id.experience -> {
                val experienceFragment =
                    ExperienceFragment()
                this.supportFragmentManager.beginTransaction()
                    .replace(R.id.content_frame, experienceFragment)
                    .addToBackStack(null)
                    .commit()
                toolbar!!.title = item.title
            }
            R.id.projects -> {
                val projectsFragment =
                    ProjectsFragment()
                this.supportFragmentManager.beginTransaction()
                    .replace(R.id.content_frame, projectsFragment)
                    .addToBackStack(null)
                    .commit()
                toolbar!!.title = item.title
            }
        }
        closeDrawer()
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }


    override fun onSupportNavigateUp(): Boolean {
        val navController =
            Navigation.findNavController(this, R.id.nav_host_fragment)
        return (NavigationUI.navigateUp(navController, appBarConfiguration!!)
                || super.onSupportNavigateUp())
    }

    override fun onBackPressed() {
        val fm = supportFragmentManager
        if (fm.backStackEntryCount > 0) {
            fm.popBackStack()
        } else {
            super.onBackPressed()
        }
    }

    private fun closeDrawer() {
        drawer!!.closeDrawers()
    }
}
