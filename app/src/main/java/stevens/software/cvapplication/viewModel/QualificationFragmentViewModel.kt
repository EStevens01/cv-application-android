package stevens.software.cvapplication.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import stevens.software.cvapplication.model.Qualification
import stevens.software.cvapplication.repository.QualificationRepository

class QualificationFragmentViewModel : ViewModel(){
    val qualifications = MutableLiveData<List<Qualification>>()

    fun getQualifications(){
        viewModelScope.launch(Dispatchers.Default) {
            qualifications.postValue(
                QualificationRepository().getQualifications())
        }
    }
}