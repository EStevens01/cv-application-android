package stevens.software.cvapplication.viewModel

import androidx.lifecycle.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import stevens.software.cvapplication.repository.AboutRepository
import stevens.software.cvapplication.model.About

class AboutFragmentViewModel : ViewModel() {

    val aboutInformation = MutableLiveData<About>()

    fun getAbout(){
        viewModelScope.launch(Dispatchers.Default) {
            aboutInformation.postValue(
                AboutRepository().fetchAbout())
        }
    }

}