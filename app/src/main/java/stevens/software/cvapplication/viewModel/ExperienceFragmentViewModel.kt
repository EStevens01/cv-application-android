package stevens.software.cvapplication.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import stevens.software.cvapplication.model.Experience
import stevens.software.cvapplication.repository.ExperienceRepository

class ExperienceFragmentViewModel : ViewModel() {

    val experiences = MutableLiveData<List<Experience>>()

    fun getExperiences(){
        viewModelScope.launch(Dispatchers.Default) {
            experiences.postValue(
                ExperienceRepository().getExperience())
        }
    }
}