package stevens.software.cvapplication.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import stevens.software.cvapplication.model.Project
import stevens.software.cvapplication.repository.ProjectRepository

class ProjectFragmentViewModel : ViewModel() {

    val projects = MutableLiveData<List<Project>>()

    fun getProjects(){
        viewModelScope.launch(Dispatchers.Default) {
            projects.postValue(
                ProjectRepository().getProjects())
        }
    }
}