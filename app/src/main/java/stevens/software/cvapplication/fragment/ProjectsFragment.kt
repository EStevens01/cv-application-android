package stevens.software.cvapplication.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import stevens.software.cvapplication.R
import stevens.software.cvapplication.model.Project
import stevens.software.cvapplication.recyclerViewAdapter.ProjectRecyclerViewAdapter
import stevens.software.cvapplication.viewModel.ProjectFragmentViewModel

class ProjectsFragment : Fragment() {
    private var projectRecyclerViewAdapter: ProjectRecyclerViewAdapter? = null
    private lateinit var linearLayoutManager: LinearLayoutManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater.inflate(R.layout.fragment_list, container, false)
        val projects = arrayListOf<Project>()

        val fragmentTitle = view.findViewById<TextView>(R.id.fragmentTitle)
        fragmentTitle.text = getString(R.string.projects_fragment_title)

        val recyclerView: RecyclerView = view.findViewById(R.id.list)
        projectRecyclerViewAdapter = ProjectRecyclerViewAdapter(context, projects)
        linearLayoutManager = LinearLayoutManager(context)
        recyclerView.adapter = projectRecyclerViewAdapter
        recyclerView.layoutManager = linearLayoutManager

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val viewModel = ViewModelProviders.of(requireActivity()).get(ProjectFragmentViewModel::class.java)
        viewModel.projects.observe(requireActivity(), Observer { projects ->
            projects?.let {
                projectRecyclerViewAdapter?.setProjects(projects as MutableList<Project>)
            }
        })
        viewModel.getProjects()
    }
}