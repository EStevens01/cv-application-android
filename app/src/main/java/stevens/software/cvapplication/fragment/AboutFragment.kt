package stevens.software.cvapplication.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import stevens.software.cvapplication.R
import stevens.software.cvapplication.viewModel.AboutFragmentViewModel

class AboutFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_about, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val viewModel = ViewModelProviders.of(requireActivity()).get(AboutFragmentViewModel::class.java)
        viewModel.aboutInformation.observe(requireActivity(), Observer {
                val name: TextView = view.findViewById(R.id.name) as TextView
                val jobTitle: TextView = view.findViewById(R.id.title) as TextView
                val description: TextView = view.findViewById(R.id.aboutDescription) as TextView

                name.text = it.name
                jobTitle.text = it.title
                description.text = it.about
        })
        viewModel.getAbout()
    }
}