package stevens.software.cvapplication.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import stevens.software.cvapplication.R
import stevens.software.cvapplication.model.Experience
import stevens.software.cvapplication.recyclerViewAdapter.ExperienceRecyclerViewAdapter
import stevens.software.cvapplication.viewModel.ExperienceFragmentViewModel

class ExperienceFragment : Fragment() {
    private var experienceRecyclerViewAdapter: ExperienceRecyclerViewAdapter? = null
    private lateinit var linearLayoutManager: LinearLayoutManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater.inflate(R.layout.fragment_list, container, false)
        val experiences = arrayListOf<Experience>()

        val fragmentTitle = view.findViewById<TextView>(R.id.fragmentTitle)
        fragmentTitle.text = getString(R.string.experience_fragment_title)

        val recyclerView: RecyclerView = view.findViewById(R.id.list)
        experienceRecyclerViewAdapter = ExperienceRecyclerViewAdapter(context, experiences)
        linearLayoutManager = LinearLayoutManager(context)
        recyclerView.adapter = experienceRecyclerViewAdapter
        recyclerView.layoutManager = linearLayoutManager

        return view;
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val viewModel = ViewModelProviders.of(requireActivity()).get(ExperienceFragmentViewModel::class.java)
        viewModel.experiences.observe(requireActivity(), Observer { experiences ->
            experiences?.let {
                experienceRecyclerViewAdapter?.setExperiences(experiences as MutableList<Experience>)
            }
        })
        viewModel.getExperiences()
    }
}