package stevens.software.cvapplication.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import stevens.software.cvapplication.R
import stevens.software.cvapplication.model.Qualification
import stevens.software.cvapplication.recyclerViewAdapter.QualificationRecyclerViewAdapter
import stevens.software.cvapplication.viewModel.QualificationFragmentViewModel

class QualificationFragment : Fragment(){
    private var qualificationRecyclerViewAdapter: QualificationRecyclerViewAdapter? = null
    private lateinit var linearLayoutManager: LinearLayoutManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater.inflate(R.layout.fragment_list, container, false)

        val fragmentTitle = view.findViewById<TextView>(R.id.fragmentTitle)
        fragmentTitle.text = getString(R.string.education_fragment_title)

        val qualifications = arrayListOf<Qualification>()
        val notesList: RecyclerView = view.findViewById(R.id.list)
        qualificationRecyclerViewAdapter = QualificationRecyclerViewAdapter(context, qualifications)
        linearLayoutManager = LinearLayoutManager(context)
        notesList.adapter = qualificationRecyclerViewAdapter
        notesList.layoutManager = linearLayoutManager

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val viewModel = ViewModelProviders.of(requireActivity()).get(QualificationFragmentViewModel::class.java)
        viewModel.qualifications.observe(requireActivity(), Observer { projects ->
            projects?.let {
                qualificationRecyclerViewAdapter?.setQualifications(projects as MutableList<Qualification>)
            }
        })
        viewModel.getQualifications()
    }
}
