package stevens.software.cvapplication.model

import java.io.Serializable

data class About(
    var name: String,
    var title: String,
    var about: String
) : Serializable
