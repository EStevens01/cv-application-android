package stevens.software.cvapplication.model

import java.io.Serializable

data class Qualification(
    val dateFrom: String,
    val dateTo: String,
    val course: String,
    val description: String
) : Serializable
