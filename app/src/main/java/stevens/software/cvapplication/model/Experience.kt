package stevens.software.cvapplication.model

import java.io.Serializable

data class Experience(
    val dateFrom: String,
    val dateTo: String,
    val title: String,
    val company: String
) : Serializable
