package stevens.software.cvapplication.model

import java.io.Serializable

data class Project(
    val title: String,
    val description: String
) : Serializable
