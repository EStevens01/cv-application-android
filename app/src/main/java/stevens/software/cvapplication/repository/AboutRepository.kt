package stevens.software.cvapplication.repository

import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import stevens.software.cvapplication.model.About
import stevens.software.cvapplication.service.AboutService

class AboutRepository {

     private fun retrofitClientAbout(): AboutService {
        return Retrofit.Builder().baseUrl("http://10.0.2.2:8080/")
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .build()
            .create(AboutService::class.java)
    }

    suspend fun fetchAbout(): About {
        return retrofitClientAbout().getAboutInformation()
    }
}
