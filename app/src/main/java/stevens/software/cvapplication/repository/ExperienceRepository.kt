package stevens.software.cvapplication.repository

import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import stevens.software.cvapplication.model.Experience
import stevens.software.cvapplication.service.ExperienceService

class ExperienceRepository {

    private fun retrofitClientExperience(): ExperienceService {
        return Retrofit.Builder().baseUrl("http://10.0.2.2:8080/")
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .build()
            .create(ExperienceService::class.java)
    }

    suspend fun getExperience(): List<Experience> {
        return retrofitClientExperience().getExperience()
    }
}