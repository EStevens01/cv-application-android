package stevens.software.cvapplication.repository

import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import stevens.software.cvapplication.model.Project
import stevens.software.cvapplication.service.ProjectService

class ProjectRepository {

    private fun retrofitClientProject(): ProjectService {
        return Retrofit.Builder().baseUrl("http://10.0.2.2:8080/")
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .build()
            .create(ProjectService::class.java)
    }

    suspend fun getProjects(): List<Project> {
        return retrofitClientProject().getProjects()
    }
}