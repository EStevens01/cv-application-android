package stevens.software.cvapplication.repository

import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import stevens.software.cvapplication.model.Qualification
import stevens.software.cvapplication.service.QualificationService

class QualificationRepository {

    private fun retrofitClientQualification(): QualificationService {
        return Retrofit.Builder().baseUrl("http://10.0.2.2:8080/")
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .build()
            .create(QualificationService::class.java)
    }

    suspend fun getQualifications(): List<Qualification> {
        return retrofitClientQualification().getQualifications()
    }
}