package stevens.software.cvapplication.service

import retrofit2.http.GET
import stevens.software.cvapplication.model.Experience

interface ExperienceService {

    @GET("experience")
    suspend fun getExperience(): List<Experience>
}