package stevens.software.cvapplication.service

import retrofit2.http.GET
import stevens.software.cvapplication.model.Qualification

interface QualificationService {

    @GET("qualifications")
    suspend fun getQualifications(): List<Qualification>
}