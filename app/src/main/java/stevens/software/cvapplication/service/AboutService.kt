package stevens.software.cvapplication.service

import retrofit2.http.GET
import stevens.software.cvapplication.model.About

interface AboutService {

    @GET("about")
    suspend fun getAboutInformation(): About

}