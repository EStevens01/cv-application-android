package stevens.software.cvapplication.service

import retrofit2.http.GET
import stevens.software.cvapplication.model.Project

interface ProjectService {

    @GET("projects")
    suspend fun getProjects(): List<Project>
}